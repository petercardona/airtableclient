package com.rathravane.airtableClient;

import java.io.IOException;
import java.util.List;

import org.json.JSONObject;

/**
 * A simple Java client for Airtable
 * @author peter@rathravane.com
 */
public interface AirtableClient
{
	/**
	 * Create a selector for the given table.
	 * @param table
	 * @return a selector
	 */
	AirtableRecordSelector createSelector ( String table );

	/**
	 * List records found via the given selector.
	 * @param ars a selector from this client's createSelector call
	 * @return a list of records
	 * @throws IOException
	 */
	List<AirtableRecord> listRecords ( AirtableRecordSelector ars ) throws IOException;

	/**
	 * Return the given record
	 * @param table
	 * @param id
	 * @return the record, or null if no such record exists
	 * @throws IOException
	 */
	AirtableRecord getRecord ( String table, String id ) throws IOException;

	/**
	 * Create a new record in the table
	 * @param table
	 * @param data
	 * @return the new record
	 * @throws IOException
	 */
	AirtableRecord createRecord ( String table, JSONObject data ) throws IOException;

	/**
	 * Create a new record in the table
	 * @param table
	 * @param id
	 * @param data
	 * @throws IOException
	 */
	void patchRecord ( String table, String id, JSONObject data ) throws IOException;

	/**
	 * Delete a record from a table by ID.
	 * @param table
	 * @param id
	 * @throws IOException
	 */
	void deleteRecord ( String table, String id ) throws IOException;
}
