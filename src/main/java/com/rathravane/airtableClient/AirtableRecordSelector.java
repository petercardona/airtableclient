package com.rathravane.airtableClient;

import java.util.List;

/**
 * A selector for an airtable record set
 * @author peter@rathravane.com
 */
public interface AirtableRecordSelector
{
	/**
	 * Get the table this selector applies to
	 * @return a table name
	 */
	String getTable ();

	/**
	 * Limit to a specific view.
	 * @param viewName
	 * @return this selector
	 */
	AirtableRecordSelector limitToView ( String viewName );

	/**
	 * Get the view set by limitToView, if any
	 * @return a view name or null
	 */
	String getView ();
	
	/**
	 * Return at most this many records.
	 * @param records
	 * @return this selector
	 */
	AirtableRecordSelector withAtMost ( int records );

	/**
	 * Get the record limit set by withAtMost
	 * return the record limit
	 */
	int getRecordLimit ();

	/**
	 * Return this many records per page.
	 * @param records
	 * @return this selector
	 */
	AirtableRecordSelector pageSize ( int records );

	/**
	 * Get the page size set by pageSize
	 * @return a page size
	 */
	int getPageSize ();

	/**
	 * Include a specific field. If this is not called,
	 * all fields are returned.
	 * @param fieldName
	 * @return this selector
	 */
	AirtableRecordSelector includeField ( String fieldName );

	/**
	 * Get the set of fields selected via includeField
	 * @return a list of fields
	 */
	List<String> getFields ();

	/**
	 * Limit with an Airtable filter
	 * @param filter
	 * @return this selector
	 */
	AirtableRecordSelector filterWith ( String filter );

	/**
	 * Get the filter set by filterWith, if any
	 * @return a filter or null
	 */
	String getFilter ();

	/**
	 * Sort with an Airtable sort
	 * @param sort
	 * @return this selector
	 */
	AirtableRecordSelector sortWith ( String sort );

	/**
	 * Get the sort set by sortWith, if any
	 * @return a sort or null
	 */
	String getSort ();
}
