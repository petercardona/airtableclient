package com.rathravane.airtableClient;

import java.time.LocalDate;
import java.util.Set;

import org.json.JSONObject;

/**
 * An airtable record
 * @author peter@rathravane.com
 */
public interface AirtableRecord
{
	/**
	 * Get the name of the table
	 * @return the table name
	 */
	String getTable ();

	/**
	 * Get the record ID
	 * @return the record ID
	 */
	String getId ();

	/**
	 * Get a string value from the record, by column name
	 * @param col
	 * @return the string value or null if the column doesn't exist
	 */
	String getString ( String col );

	/**
	 * Get a string value from the record, by column name
	 * @param col the column name
	 * @param defval a default value
	 * @return the string value or the default value if the column doesn't exist
	 */
	String getString ( String col, String defval );

	/**
	 * Get an int value from the record, by column name
	 * @param col the column name
	 * @return the value or 0 if the column doesn't exist
	 */
	int getInt ( String col );

	/**
	 * Get an int value from the record, by column name
	 * @param col the column name
	 * @param defval a default value
	 * @return the value or the default value if the column doesn't exist
	 */
	int getInt ( String col, int defval );

	/**
	 * Get a date value from the record, by column name
	 * @param col the column name
	 * @return the value or null if the column doesn't exist
	 */
	LocalDate getDate ( String col );

	/**
	 * Get a date value from the record, by column name
	 * @param col the column name
	 * @param defval a default value
	 * @return the value or the default value if the column doesn't exist
	 */
	LocalDate getDate ( String col, LocalDate defval );
	
	/**
	 * Get a timestamp value from the record, by column name
	 * @param col the column name
	 * @return the value or null if the column doesn't exist
	 */
	long getTimestamp ( String col );

	/**
	 * Get a timestamp value from the record, by column name
	 * @param col the column name
	 * @param defval a default value
	 * @return the value or the default value if the column doesn't exist
	 */
	long getTimestamp ( String col, long defval );

	/**
	 * Get a set of values from a multiple value column.
	 * @param col the column name
	 * @return a set of values, which may be empty
	 */
	Set<String> getSet ( String col );

	/**
	 * Get a set of values from a multiple value column.
	 * @param col the column name
	 * @param defval a default value
	 * @return a set of values, which may be empty
	 */
	Set<String> getSet ( String col, Set<String> defval );
	
	/**
	 * Get the raw JSON value for the given column
	 * @param col
	 * @return a value or null
	 */
	Object getRawValue ( String col );

	/**
	 * Get the raw JSON value for the given column
	 * @param col
	 * @param defval
	 * @return a value or the default value
	 */
	Object getRawValue ( String col, Object defval );

	/**
	 * Serialize this record into JSON
	 * @return a JSON object
	 */
	JSONObject asJson ();
}
