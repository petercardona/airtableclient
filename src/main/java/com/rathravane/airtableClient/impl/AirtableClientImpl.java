package com.rathravane.airtableClient.impl;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.TreeSet;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.rathravane.airtableClient.AirtableClient;
import com.rathravane.airtableClient.AirtableClientFactory;
import com.rathravane.airtableClient.AirtableRecord;
import com.rathravane.airtableClient.AirtableRecordSelector;

public class AirtableClientImpl implements AirtableClient
{
	public AirtableClientImpl ( AirtableClientFactory factory )
	{
		fApiKey = factory.getApiKey ();
		fBaseId = factory.getBase ();
	}

	@Override
	public AirtableRecord getRecord ( String table, String id ) throws IOException
	{
		final String path = new StringBuilder ()
			.append ( encode ( table ) )
			.append ( "/" )
			.append ( encode ( id ) )
			.toString ()
		;

		final HttpResponse<JsonNode> res = runApiGet ( path, null );
		if ( res.getStatus() == 200 )
		{
			final JSONObject rec = res.getBody().getObject ();
			return new AirtableRecordImpl ( table, rec );
		}
		else if ( res.getStatus() == 404 )
		{
			return null;
		}
		throw new IOException ( res.getStatusText () );
	}

	@Override
	public Selector createSelector ( String table )
	{
		return new Selector ( table );
	}

	private String encode ( String in )
	{
		// airtable can't handle "+" as space, just "%20"
		return in.replaceAll ( "\\s", "%20" );
	}
	
	@Override
	public List<AirtableRecord> listRecords ( final AirtableRecordSelector ars ) throws IOException
	{
		final LinkedList<AirtableRecord> result = new LinkedList<> ();

		final String path = new StringBuilder ()
			.append ( encode ( ars.getTable () ) )
			.toString ()
		;

		final HashMap<String,String> args = new HashMap<> ();
		String offset = null;

		final String view = ars.getView ();
		if ( view != null ) args.put ( "view", view );

		final String filter = ars.getFilter ();
		if ( filter != null ) args.put ( "filterByFormula", filter );

		final String sort = ars.getSort ();
		if ( sort != null ) args.put ( "sort", sort );

		final int pageSize = ars.getPageSize ();
		if ( pageSize > 0 ) args.put ( "pageSize", "" + pageSize );
		
		final int recLimit = ars.getRecordLimit ();
		if ( recLimit > 0 ) args.put ( "maxRecords", "" + recLimit );

		final List<String> fields = ars.getFields ();
		if ( fields != null )
		{
			final JSONArray fieldArr = new JSONArray ();
			for ( String field : fields )
			{
				fieldArr.put ( field );
			}
			args.put ( "fields", fieldArr.toString () );
		}

		boolean restart = false;
		do
		{
			restart = false;

			if ( offset != null )
			{
				args.put ( "offset", offset );
			}

			final HttpResponse<JsonNode> res = runApiGet ( path, args );
			if ( res.getStatus() == 200 )
			{
				offset = res.getBody ().getObject ().optString ( "offset", null );

				final JSONArray recs = res.getBody ().getObject ().optJSONArray ( "records" );
				for ( int i=0; i<recs.length (); i++ )
				{
					final JSONObject rec = recs.getJSONObject ( i );
					result.add ( new AirtableRecordImpl ( ars.getTable (), rec ) );
				}
			}
			else if ( res.getStatus() == 422 )
			{
				try
				{
					final String errorText = res.getBody ().getObject ()
						.getJSONObject ( "error" ).getString ( "type" )
					;
					if ( errorText.equals ( "LIST_RECORDS_ITERATOR_NOT_AVAILABLE" ) )
					{
						offset = null;
						restart = true;
						result.clear ();
					}
				}
				catch ( JSONException x )
				{
					// ignore it...
				}
			}
		}
		while ( restart || offset != null );

		return result;
	}

	public AirtableRecord createRecord ( String table, JSONObject data ) throws IOException
	{
		final String path = new StringBuilder ()
			.append ( encode ( table ) )
			.toString ()
		;

		final JSONObject dataToPost = new JSONObject ()
			.put ( "fields", data )
		;
		final HttpResponse<JsonNode> res = runApiPost ( path, dataToPost );
		if ( res.getStatus() >= 200 && res.getStatus() < 300 )
		{
			return new AirtableRecordImpl ( table, res.getBody ().getObject () );
		}

		throw new IOException ( res.getStatusText () );
	}
	
	public void deleteRecord ( String table, String id ) throws IOException
	{
		final String path = new StringBuilder ()
			.append ( encode ( table ) )
			.append ( "/" )
			.append ( encode ( id ) )
			.toString ()
		;

		final HttpResponse<JsonNode> res = runApiDelete ( path );
		if ( res.getStatus() >= 400 )
		{
			throw new IOException ( res.getStatusText () );
		}
	}

	public void patchRecord ( String table, String id, JSONObject data ) throws IOException
	{
		final String path = new StringBuilder ()
			.append ( encode ( table ) )
			.append ( "/" )
			.append ( id )
			.toString ()
		;

		final JSONObject dataToPost = new JSONObject ()
			.put ( "fields", data )
		;
		final HttpResponse<JsonNode> res = runApiPatch ( path, dataToPost );
		if ( res.getStatus() >= 200 && res.getStatus() < 300 )
		{
//			return new AirtableRecordImpl ( table, res.fData );
			return;
		}

		throw new IOException ( res.getStatusText () );
	}

	private final String fApiKey;
	private final String fBaseId;
	
	private static final String kBaseUrl = "https://api.airtable.com/v0/";

	private static class Selector implements AirtableRecordSelector
	{
		public Selector ( String table )
		{
			fTable = table;
		}

		@Override
		public String getTable () { return fTable; }

		@Override
		public AirtableRecordSelector limitToView ( String viewName )
		{
			fView = viewName;
			return this;
		}

		@Override
		public String getView () { return fView; }

		@Override
		public int getRecordLimit () { return fRecordLimit; }

		@Override
		public int getPageSize () { return fPageSize; }

		@Override
		public List<String> getFields () { return fFields; }

		@Override
		public AirtableRecordSelector includeField ( String fieldName )
		{
			if ( fFields == null )
			{
				fFields = new LinkedList<> ();
			}
			fFields.add ( fieldName );
			return this;
		}

		@Override
		public AirtableRecordSelector withAtMost ( int records )
		{
			fRecordLimit = records;
			return this;
		}

		@Override
		public AirtableRecordSelector pageSize ( int records )
		{
			fPageSize = records;
			return this;
		}

		@Override
		public AirtableRecordSelector filterWith ( String filter )
		{
			fFilter = filter;
			return this;
		}

		@Override
		public String getFilter () { return fFilter; }

		@Override
		public AirtableRecordSelector sortWith ( String sort )
		{
			fSort = sort;
			return this;
		}

		@Override
		public String getSort () { return fSort; }

		private final String fTable;
		private String fView = null;
		private LinkedList<String> fFields = null;
		private int fRecordLimit = -1;
		private int fPageSize = -1;
		private String fSort = null;
		private String fFilter = null;
	}

	private static Map<String,Object> convertMap ( Map<String,String> map )
	{
		if ( map == null ) return null;
		final HashMap<String,Object> result = new HashMap<> ();
		for ( Map.Entry<String, String> e : map.entrySet () )
		{
			result.put ( e.getKey (), e.getValue () );
		}
		return result;
	}

	private HttpResponse<JsonNode> runApiGet ( String path, Map<String, String> args ) throws IOException
	{
		try
		{
			final StringBuilder sb = new StringBuilder ()
				.append ( kBaseUrl )
				.append ( fBaseId )
				.append ( '/' )
				.append ( path )
			;

			return Unirest.get ( sb.toString () )
				.header ( "Authorization", "Bearer " + fApiKey )
				.header ( "Content-type", "application/json" )
				.header ( "accept", "application/json" )
				.queryString ( convertMap ( args ) )
				.asJson ()
			;
		}
		catch ( UnirestException x )
		{
			throw new IOException ( x );
		}
	}

	private HttpResponse<JsonNode> runApiPost ( String path, JSONObject data ) throws IOException
	{
		try
		{
			final StringBuilder sb = new StringBuilder ()
				.append ( kBaseUrl )
				.append ( fBaseId )
				.append ( '/' )
				.append ( path )
			;

			return Unirest.post ( sb.toString () )
				.header ( "Authorization", "Bearer " + fApiKey )
				.header ( "Content-type", "application/json" )
				.header ( "accept", "application/json" )
				.body ( data )
				.asJson ()
			;
		}
		catch ( UnirestException x )
		{
			throw new IOException ( x );
		}
	}

	private HttpResponse<JsonNode> runApiPatch ( String path, JSONObject data ) throws IOException
	{
		try
		{
			final StringBuilder sb = new StringBuilder ()
				.append ( kBaseUrl )
				.append ( fBaseId )
				.append ( '/' )
				.append ( path )
			;

			return Unirest.patch ( sb.toString () )
				.header ( "Authorization", "Bearer " + fApiKey )
				.header ( "Content-type", "application/json" )
				.header ( "accept", "application/json" )
				.body ( data )
				.asJson ()
			;
		}
		catch ( UnirestException x )
		{
			throw new IOException ( x );
		}
	}

	private HttpResponse<JsonNode> runApiDelete ( String path ) throws IOException
	{
		try
		{
			final StringBuilder sb = new StringBuilder ()
				.append ( kBaseUrl )
				.append ( fBaseId )
				.append ( '/' )
				.append ( path )
			;

			return Unirest.delete ( sb.toString () )
				.header ( "Authorization", "Bearer " + fApiKey )
				.header ( "Content-type", "application/json" )
				.header ( "accept", "application/json" )
				.asJson ()
			;
		}
		catch ( UnirestException x )
		{
			throw new IOException ( x );
		}
	}

	private class AirtableRecordImpl implements AirtableRecord
	{
		public AirtableRecordImpl ( String table, JSONObject rec )
		{
			fTable = table;
			fData = rec;
			fFields = fData.getJSONObject ( "fields" );
		}

		@Override
		public String toString () { return getId () + " (" + fTable + ")"; }

		@Override
		public String getTable () { return fTable; }
		
		@Override
		public String getId () { return fData.getString ( "id" ); }

		@Override
		public String getString ( String col )
		{
			return getString ( col, null );
		}

		@Override
		public String getString ( String col, String defval )
		{
			return fFields.optString ( col, defval );
		}

		@Override
		public int getInt ( String col )
		{
			return getInt ( col, 0 );
		}

		@Override
		public int getInt ( String col, int defval )
		{
			return fFields.optInt ( col, defval );
		}

		@Override
		public LocalDate getDate ( String col )
		{
			return getDate ( col, null );
		}

		@Override
		public LocalDate getDate ( String col, LocalDate defval )
		{
			final String val = getString ( col, "" );
			if ( val.length () == 0 )
			{
				return defval;
			}

			try
			{
				return LocalDate.parse ( val );
			}
			catch ( DateTimeParseException e )
			{
				return defval;
			}
		}

		@Override
		public long getTimestamp ( String col )
		{
			return getTimestamp ( col, 0L );
		}

		@Override
		public long getTimestamp ( String col, long defval )
		{
			final String val = getString ( col, "" );
			if ( val.length () == 0 )
			{
				return defval;
			}

			try
			{
				if ( val.length () == 10 )
				{
					final SimpleDateFormat sdf = new SimpleDateFormat ( "yyyy-MM-dd" );
					sdf.setTimeZone ( TimeZone.getTimeZone ( "UTC" ) );
					final Date d = sdf.parse ( val );
					final Calendar cal = Calendar.getInstance ();
					cal.setTime ( d );
					cal.setTimeZone ( TimeZone.getTimeZone ( "UTC" ) );
					cal.set ( Calendar.HOUR_OF_DAY, 12 );
					cal.set ( Calendar.MINUTE, 0 );
					cal.set ( Calendar.SECOND, 0 );
					cal.set ( Calendar.MILLISECOND, 0 );
					return cal.getTimeInMillis ();
				}
				else
				{
					final SimpleDateFormat sdf = new SimpleDateFormat ( "yyyy-MM-dd'T'hh:mm:ss.SSS'Z'" );
					sdf.setTimeZone ( TimeZone.getTimeZone ( "UTC" ) );
					final Date d = sdf.parse ( val );
					return d.getTime ();
				}
			}
			catch ( ParseException e )
			{
				return defval;
			}
		}

		@Override
		public Set<String> getSet ( String col )
		{
			return getSet ( col, null );
		}

		@Override
		public Set<String> getSet ( String col, Set<String> defval )
		{
			final JSONArray val = fFields.optJSONArray ( col );
			if ( val == null )
			{
				return defval;
			}

			final TreeSet<String> result = new TreeSet<> ();
			final int len = val.length ();
			for ( int i=0; i<len; i++ )
			{
				final Object o = val.get ( i );
				if ( o != null )
				{
					result.add ( o.toString () );
				}
			}
			return result;
		}

		@Override
		public Object getRawValue ( String col )
		{
			return getRawValue ( col, null );
		}

		@Override
		public Object getRawValue ( String col, Object defval )
		{
			final Object result = fFields.opt ( col );
			if ( result == null ) return defval;
			return result;
		}

		@Override
		public JSONObject asJson()
		{
			// this is a goofy clone but it's simple
			return new JSONObject ( new JSONTokener ( fData.toString() ) );
		}

		private final String fTable;
		private final JSONObject fData;
		private final JSONObject fFields;
	}
}
