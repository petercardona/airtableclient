package com.rathravane.airtableClient;

import java.io.IOException;

import com.rathravane.airtableClient.impl.AirtableClientImpl;

/**
 * A factory for creating an airtable client
 * @author peter@rathravane.com
 */
public class AirtableClientFactory
{
	/**
	 * Specify the API key for the connection
	 * @param apiKey
	 * @return this factory
	 */
	public AirtableClientFactory withApiKey ( String apiKey )
	{
		fApiKey = apiKey;
		return this;
	}

	/**
	 * Specify the Airtable base name
	 * @param baseName
	 * @return this factory
	 */
	public AirtableClientFactory usingBase ( String baseName )
	{
		fBase = baseName;
		return this;
	}

	/**
	 * Build a client instance.
	 * @return a Buxfer client
	 * @throws IOException 
	 */
	public AirtableClient build ()
	{
		if ( fApiKey == null )
		{
			throw new IllegalArgumentException ( "Please set the API key before building the client." );
		}
		if ( fBase == null )
		{
			throw new IllegalArgumentException ( "Please set the base name before building the client." );
		}
		return new AirtableClientImpl ( this );
	}

	private String fApiKey;
	private String fBase;

	public String getApiKey () { return fApiKey; }
	public String getBase () { return fBase; }
}
